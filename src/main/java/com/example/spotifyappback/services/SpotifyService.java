package com.example.spotifyappback.services;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.example.spotifyappback.configuration.SpotifyConfig;
import com.example.spotifyappback.model.spotify.SpotifyAlbum;
import com.example.spotifyappback.model.spotify.SpotifySearchResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class SpotifyService {
    
    private SpotifyConfig config;
    private WebClient clientSpotify;

    private String apiUrl = "https://api.spotify.com/v1";

    @Autowired
    public SpotifyService(SpotifyConfig _config){
        this.config = _config;

        clientSpotify = WebClient.builder()
            .baseUrl(apiUrl)
            .build();
    }

    //curl -X "GET" "https://api.spotify.com/v1/search?q=artist%3AMiles&type=album%2Cartist&limit=20" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer BQCQSqY9Qs_QTCq2RvGu12iB_oWJc_t1AMn0h56ZP8stO6ROdLwjleEM1RzLnEYnMm9eTqJLksX8NzouV-FNzbRNWDoYhWvfftbc__oomi2SkMGoc4BsNsFaqzTdem_PcjMvfdPhFTI4XHirDhXMg85LL5qBYgT244g"

    public List<SpotifyAlbum> search(String stringToSearch){
        // TODO: Mettre la génération de la requête dans une méthode à part
        SpotifySearchResult result = clientSpotify.get()
            .uri(uriBuilder -> uriBuilder
                .path("search")
                .queryParam("q", String.format("album:%s", stringToSearch))
                .queryParam("type", "album")
                .queryParam("limit", 20)
                .build())
            .header(HttpHeaders.ACCEPT, "application/json")
            .header(HttpHeaders.CONTENT_TYPE, "application/json")
            .header(HttpHeaders.AUTHORIZATION, "Bearer " + config.getAuthToken())
            .retrieve()
            .bodyToMono(SpotifySearchResult.class)
            .block();

        // S'il n'y a pas 20 résultats, on relance la recherche mais en cherchant les artistes
        if(result.getAlbums().getTotal() == 20){
            return result.getAlbums().getItems();
        }
        else{
            SpotifySearchResult resultWithArtist = clientSpotify.get()
            .uri(uriBuilder -> uriBuilder
                .path("search")
                .queryParam("q", String.format("artist:%s", stringToSearch))
                .queryParam("type", "album")
                .queryParam("limit", 20 - result.getAlbums().getTotal())
                .build())
            .header(HttpHeaders.ACCEPT, "application/json")
            .header(HttpHeaders.CONTENT_TYPE, "application/json")
            .header(HttpHeaders.AUTHORIZATION, "Bearer " + config.getAuthToken())
            .retrieve()
            .bodyToMono(SpotifySearchResult.class)
            .block();

            return Stream.concat(
                result.getAlbums().getItems().stream(),
                resultWithArtist.getAlbums().getItems().stream())
                .collect(Collectors.toList());
        }
    }
}
