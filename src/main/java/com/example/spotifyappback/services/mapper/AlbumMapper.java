package com.example.spotifyappback.services.mapper;

import java.util.List;

import com.example.spotifyappback.model.AlbumEntity;
import com.example.spotifyappback.services.dto.AlbumDto;

import org.mapstruct.Mapper;


@Mapper
public interface AlbumMapper {

    // entity to dto
    AlbumDto entityToDto(AlbumEntity entity);
    List<AlbumDto> entitiesToDtos(List<AlbumEntity> entity);

    
    // dto to entity
    AlbumEntity dtoToEntity(AlbumDto dto);
}