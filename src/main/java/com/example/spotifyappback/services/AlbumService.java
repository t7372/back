package com.example.spotifyappback.services;

import java.util.List;

import com.example.spotifyappback.repository.AlbumRepository;
import com.example.spotifyappback.services.dto.AlbumDto;
import com.example.spotifyappback.services.mapper.AlbumMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AlbumService {
    
    @Autowired
    private SpotifyService spotifyService;

    @Autowired
    private AlbumRepository albumRepository;

    @Autowired
    private AlbumMapper albumMapper;

    // public SpotifyAlbum

    public List<AlbumDto> getAll(){
        return albumMapper.entitiesToDtos(albumRepository.getAll());
    }
}
