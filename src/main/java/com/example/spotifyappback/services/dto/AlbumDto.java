package com.example.spotifyappback.services.dto;

import java.time.LocalDate;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AlbumDto {
    private String id;
    private String name;
    private List<AlbumImageDto> images;
    private LocalDate release_date;
}
