package com.example.spotifyappback.controller;

import com.example.spotifyappback.model.AlbumEntity;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
class AlbumController{

    @GetMapping(value = "/albums", produces = "application/json")
    public ResponseEntity<AlbumEntity> all() {
        log.info("C'est passé par là");

        AlbumEntity result = AlbumEntity.builder()
            .name("Got it")
            .build();


        return ResponseEntity.ok(result);
    }
}