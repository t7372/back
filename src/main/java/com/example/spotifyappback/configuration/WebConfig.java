package com.example.spotifyappback.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        // Désactive la protection Cross-Origin sur le projet.
        // Bien évidemment il s'agit d'une énorme faille de sécurité qui ne doit pas être laissé tel quel dans un vrai projet.
        registry.addMapping("/**");
    }
}
