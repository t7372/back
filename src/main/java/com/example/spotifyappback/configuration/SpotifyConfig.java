package com.example.spotifyappback.configuration;

import java.util.Base64;
import java.util.Timer;
import java.util.TimerTask;

import com.example.spotifyappback.model.AuthTokenData;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class SpotifyConfig {

    private WebClient clientSpotify;
    private String tokenApiPath = "/api/token";

    private String accountApiUrl = "https://accounts.spotify.com";
    private String clientId = "414910b26d8d45c4b82e931db8db12f4";
    private String clientSecret = "98bcdb5f4ad34d8aaf3ac70d10958c0a";

    @Getter
    private String authToken;

    private Timer tokenRefreshTimer;

    public SpotifyConfig(){
        log.info("Initialisation de la gestion du token Spotify");
        clientSpotify = WebClient.builder()
            .baseUrl(accountApiUrl)
            .build();

        tokenRefreshTimer = new Timer();
        
        reloadAuthToken();
    }

    public void reloadAuthToken(){

        log.info("Préparation de la requête de récupération du token");

        String authString = clientId + ":" + clientSecret;
        String encodeAuthString = Base64.getEncoder().encodeToString(authString.getBytes());
        String authorizationValue = "Basic " + encodeAuthString;                                                                                      

        AuthTokenData token = clientSpotify.post()
        .uri(tokenApiPath)
        .body(BodyInserters.fromFormData("grant_type", "client_credentials"))
        .header("Authorization", authorizationValue)
        .header(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded")
        .retrieve()
        .bodyToMono(AuthTokenData.class)
        .block();

        authToken = token.getAccess_token();

        tokenRefreshTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                reloadAuthToken();
            }
        }, token.getExpires_in()); // Peut être prévoir un delta pour récupérer un nouveau token avant la fin du chrono pour éviter d'avoir des coupure entre deux recharges
    }
}
