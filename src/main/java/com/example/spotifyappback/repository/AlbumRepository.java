package com.example.spotifyappback.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.spotifyappback.model.AlbumEntity;

import org.springframework.stereotype.Repository;

//Devrait étendre JpaRepository et permettre la connexion avec une bdd. Pour simplifier ce "repo" maintiendra une map en guise de base de donnée d'album dont la clé sera l'id de l'album (sensé faire office de clé primaire)
@Repository
public class AlbumRepository {
    private Map<String, AlbumEntity> albums;

    public AlbumRepository(){
        albums = new HashMap<>();
    }

    public List<AlbumEntity> getAll(){
        return List.copyOf(albums.values());
    }

    public AlbumEntity save(AlbumEntity album){
        return albums.put(album.getId(), album);
    }

    public AlbumEntity delete(String id){
        return albums.remove(id);
    }
}
