package com.example.spotifyappback.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AuthTokenData {
    private String access_token;
    private String token_type;
    private int expires_in;
}