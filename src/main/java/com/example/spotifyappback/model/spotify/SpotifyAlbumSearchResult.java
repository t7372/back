package com.example.spotifyappback.model.spotify;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SpotifyAlbumSearchResult {
    private List<SpotifyAlbum> items;
    private int total;
}