package com.example.spotifyappback.model.spotify;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SpotifySearchResult {
    private SpotifyAlbumSearchResult albums;
}