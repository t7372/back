package com.example.spotifyappback.model.spotify;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SpotifyAlbum {
    private String id;
    private String name;
    private List<SpotifyAlbumImage> images;
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate release_date;
}