package com.example.spotifyappback.services;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.*;

import java.util.List;

import com.example.spotifyappback.model.AlbumEntity;
import com.example.spotifyappback.repository.AlbumRepository;
import com.example.spotifyappback.services.dto.AlbumDto;
import com.example.spotifyappback.services.mapper.AlbumMapper;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AlbumServiceTest {
    
    @InjectMocks
    private AlbumService albumService;
    
    @Mock
    private SpotifyService spotifyService;

    @Mock
    private AlbumRepository albumRepository;

    @Mock
    private AlbumMapper albumMapper;

    @Nested
    class getAll{

        @Test
        public void call_repository_getAll(){
            // Arrange

            // Act
            albumService.getAll();

            // Assert
            verify(albumRepository).getAll();
        }

        @Test
        public void call_mapper_entitiesToDtos(){
            // Arrange
            List<AlbumEntity> bddList = List.of();

            when(albumRepository.getAll()).thenReturn(bddList);

            // Act
            albumService.getAll();

            // Assert
            verify(albumMapper).entitiesToDtos(bddList);
        }

        @Test
        public void return_repository_getAll(){
            // Arrange
            List<AlbumDto> expectedResult = List.of();
            List<AlbumEntity> bddList = List.of();

            when(albumRepository.getAll()).thenReturn(bddList);
            when(albumMapper.entitiesToDtos(bddList)).thenReturn(expectedResult);

            // Act
            List<AlbumDto> result = albumService.getAll();

            // Assert
            assertThat(result).isEqualTo(expectedResult);
        }
    } 
}
